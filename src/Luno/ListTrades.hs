{-# LANGUAGE OverloadedStrings #-}

module Luno.ListTrades where

import Data.Aeson

import qualified Data.ByteString as BS

import Luno.Common

data Trade = Trade
  { tradeBase           :: Float    -- ^ Amount in Base Currency (eg BTC)
  , tradeClientOrderId  :: String   -- ^ Order ID supplied by client
  , tradeCounter        :: Float    -- ^ Amount on other side of trade (eg ZAR)
  , tradeFeeBase        :: Float    -- ^ Fee amount in base currency
  , tradeFeeCounter     :: Float    -- ^ Fee amount in counter currency
  , tradeIsBuy          :: Bool     -- ^ True if trade is a take, False if make?
  , tradeOrderId        :: String   -- ^ Luno order ID
  , tradePair           :: String   -- ^ The pair, eg XBTZAR
  , tradePrice          :: Float    -- ^ Trade price
  , tradeSequence       :: Int      -- ^ Possibly for before_seq and after_seq
  , tradeTimestamp      :: Int      -- ^ Timestamp of trade
  , tradeType           :: String   -- ^ eg BID
  , tradeVolume         :: Float    -- ^ Volume in base currency
  } deriving (Eq, Show)

instance FromJSON Trade where
  parseJSON (Object o) =
    Trade <$> (read <$> o .: "base")
          <*> o .: "client_order_id"
          <*> (read <$> o .: "counter")
          <*> (read <$> o .: "fee_base")
          <*> (read <$> o .: "fee_counter")
          <*> o .: "is_buy"
          <*> o .: "order_id"
          <*> o .: "pair"
          <*> (read <$> o .: "price")
          <*> o .: "sequence"
          <*> o .: "timestamp"
          <*> o .: "type"
          <*> (read <$> o .: "volume")

newtype Trades = Trades
  { trades  :: [ Trade ]
  } deriving (Eq, Show)

instance FromJSON Trades where
  parseJSON (Object o) = Trades <$> o .: "trades"

type Option = (BS.ByteString, Maybe BS.ByteString)

-- add option to list of options if it doesn't contain they key already
addOption :: BS.ByteString -> BS.ByteString -> [ Option ] -> [ Option ]
addOption key value options
  | key `elem` map fst options  = options
  | otherwise                   = (key, Just value) : options

listTrades :: LunoClient -> [ Option ] -> IO [ Trade ]
listTrades client options = do
  result <- lunoAuthenticatedQuery client "1/listtrades" options'
  return $ trades result
  where options'  = addOption "pair" "XBTZAR" $ addOption "limit" "100" options
