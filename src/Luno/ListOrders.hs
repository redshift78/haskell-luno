{-# LANGUAGE OverloadedStrings #-}

module Luno.ListOrders where

import Data.Aeson
import Data.ByteString.Char8 (pack)

import Luno.Common

data Order = Order
  { orderBase                 :: String   -- ^ Amount in Base Currency (eg BTC)
  , orderBaseAccountId        :: Int      -- ^ Not sure, a wallet for the base currency?
  , orderClientOrderId        :: String   -- ^ An order ID supplied by client when creating it, or empty
  , orderCompletedTimestamp   :: Int      -- ^ Timestamp of when order completed
  , orderCounter              :: String   -- ^ Amount on other side of pair (eg ZAR)
  , orderCounterAccountId     :: Int      -- ^ Not sure, wallet for other side of pair currency?
  , orderCreationTimestamp    :: Int      -- ^ Timestamp of when order was created
  , orderExpirationTimestamp  :: Int      -- ^ Timestamp of when order expires, or 0
  , orderFeeBase              :: String   -- ^ Fee amount in base currency
  , orderFeeCounter           :: String   -- ^ Fee amount in counter currency
  , orderLimitPrice           :: String   -- ^ The limit price in the counter currency
  , orderLimitVolume          :: String   -- ^ How much of the base currency to buy/sell
  , orderOrderId              :: String   -- ^ The Luno order ID
  , orderPair                 :: String   -- ^ The order pair, eg XBTZAR
  , orderSide                 :: String   -- ^ BUY / SELL
  , orderStatus               :: String   -- ^ COMPLETE | AWAITING | PENDING
  -- , orderStopDirection        :: String
  -- , orderStopPrice            :: String
  , orderTimeInForce          :: String   -- ^ GTC ?
  , order_type                :: String   -- ^ LIMIT
  } deriving (Eq, Show)

instance FromJSON Order where
  parseJSON (Object o) =
    Order <$> o .: "base"
          <*> o .: "base_account_id"
          <*> o .: "client_order_id"
          <*> o .: "completed_timestamp"
          <*> o .: "counter"
          <*> o .: "counter_account_id"
          <*> o .: "creation_timestamp"
          <*> o .: "expiration_timestamp"
          <*> o .: "fee_base"
          <*> o .: "fee_counter"
          <*> o .: "limit_price"
          <*> o .: "limit_volume"
          <*> o .: "order_id"
          <*> o .: "pair"
          <*> o .: "side"
          <*> o .: "status"
          -- <*> o .: "stop_direction"
          -- <*> o .: "stop_price"
          <*> o .: "time_in_force"
          <*> o .: "type"

newtype Orders = Orders
  { orders  :: [ Order ]
  } deriving (Eq, Show)

instance FromJSON Orders where
  parseJSON (Object o) = Orders <$> o .: "orders"

listOrders :: Bool -> LunoClient -> IO [ Order ]
listOrders closed client = do
  result <- lunoAuthenticatedQuery client "exchange/2/listorders" [ ("closed", closed') ]
  return $ orders result
  where closed' | closed    = Just "true"
                | otherwise = Just "false"

listOpenOrders    = listOrders False
listClosedOrders  = listOrders True

orderDetails :: LunoClient -> String -> IO Order
orderDetails client orderId = lunoAuthenticatedQuery client "exchange/3/order" [ ("id", orderId') ]
  where orderId'  = Just $ pack orderId

-- | Cancel order, return True if successful
cancelOrder :: LunoClient -> String -> IO ()
cancelOrder client orderId = lunoAuthenticatedPost client "1/stoporder" [ ("order_id", orderId') ]
  where orderId'  = Just $ pack orderId
