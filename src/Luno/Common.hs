module Luno.Common where

import Data.Aeson
import Data.ByteString
import Network.HTTP.Client
import Network.HTTP.Simple
import Data.Maybe (fromJust)

data LunoClient = LunoClient
  { _publicBaseUrl    :: String
  , _key              :: Maybe ByteString
  , _secret           :: Maybe ByteString
  } deriving (Eq, Show)

newLunoClient :: LunoClient
newLunoClient = LunoClient "https://api.luno.com/api/" Nothing Nothing

newAuthenticatedLunoClient :: ByteString -> ByteString -> LunoClient
newAuthenticatedLunoClient key secret = newLunoClient { _key = Just key, _secret = Just secret }

lunoQuery :: (FromJSON a) => LunoClient -> String -> [ (ByteString, Maybe ByteString) ] -> IO a
lunoQuery client endpoint params = do
  request <- parseRequest $ _publicBaseUrl client <> endpoint
  return <$> getResponseBody =<< httpJSON (setQueryString params request)

lunoAuthenticatedQuery :: (FromJSON a) => LunoClient -> String -> [ (ByteString, Maybe ByteString) ] -> IO a
lunoAuthenticatedQuery client endpoint params = do
  request <- parseRequest $ _publicBaseUrl client <> endpoint
  return <$> getResponseBody =<< httpJSON (applyBasicAuth key secret $ setQueryString params request)
  where key     = fromJust $ _key client
        secret  = fromJust $ _secret client

lunoAuthenticatedPost :: (FromJSON a) => LunoClient -> String  -> [ (ByteString, Maybe ByteString) ] -> IO a
lunoAuthenticatedPost client endpoint params = do
  request <- parseRequest $ "POST " <> _publicBaseUrl client <> endpoint
  return <$> getResponseBody =<< httpJSON (applyBasicAuth key secret $ setQueryString params request)
  where key     = fromJust $ _key client
        secret  = fromJust $ _secret client
