{-# LANGUAGE OverloadedStrings #-}

module Luno.Ticker where

import Data.Aeson
import Data.ByteString

import Luno.Common

data Ticker = Ticker
  { pair                    :: String
  , timestamp               :: Int
  , bid                     :: Float
  , ask                     :: Float
  , last_trade              :: Float
  , rolling_24_hour_volume  :: Float
  , status                  :: String
  } deriving (Eq, Show)

instance FromJSON Ticker where
  parseJSON (Object o) =
    Ticker  <$> o .: "pair"
            <*> o .: "timestamp"
            <*> (read <$> o .: "bid")
            <*> (read <$> o .: "ask")
            <*> (read <$> o .: "last_trade")
            <*> (read <$> o .: "rolling_24_hour_volume")
            <*> o .: "status"

ticker :: LunoClient -> ByteString -> IO Ticker
ticker client pair = lunoQuery client "1/ticker" [ ("pair", Just pair) ]
