{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Luno.PostLimitOrder where

import Data.Aeson
import GHC.Generics
import Data.ByteString
import Data.ByteString.UTF8 (fromString)
import Numeric (showFFloat)

import Luno.Common

data OrderType = OrderBid | OrderAsk deriving (Eq, Show)

data LimitOrder = LimitOrder
  { limitOrderPair      :: ByteString
  , limitOrderType      :: OrderType
  , limitOrderPostOnly  :: Bool
  , limitOrderVolume    :: Float  -- Amount of crypto to buy or sell
  , limitOrderPrice     :: Int    -- Price in base currency (eg ZAR)
  } deriving (Eq, Show)

newtype OrderResponse = OrderResponse
  { order_id  :: String
  } deriving (Eq, Show, Generic)

instance FromJSON OrderResponse

newOrder :: ByteString -> OrderType -> Bool -> Float -> Int -> LimitOrder
newOrder pair otype postonly volume price = LimitOrder
  { limitOrderPair      = pair
  , limitOrderType      = otype
  , limitOrderPostOnly  = postonly
  , limitOrderVolume    = volume
  , limitOrderPrice     = price
  }

postLimitOrder :: LunoClient -> LimitOrder -> IO OrderResponse
postLimitOrder client order = lunoAuthenticatedPost client "1/postorder"
  [ ("pair", Just pair)
  , ("type", Just otype)
  , ("post_only", Just postonly)
  , ("volume", Just volume)
  , ("price", Just price)
  ]
  where pair      = limitOrderPair order
        otype     | limitOrderType order == OrderBid = "BID"
                  | limitOrderType order == OrderAsk = "ASK"
        postonly  | limitOrderPostOnly order = "true"
                  | otherwise           = "false"
        volume    = fromString $ showFFloat (Just 6) (limitOrderVolume order - 0.0000005) ""
        price     = fromString $ show $ limitOrderPrice order
