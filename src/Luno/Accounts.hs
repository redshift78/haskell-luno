{-# LANGUAGE OverloadedStrings #-}

module Luno.Accounts
  ( Balance (..)
  , getAccountBalances
  ) where

import Data.Aeson

import Luno.Common

data Balance = Balance
  { balanceAccId        :: String
  , balanceAsset        :: String
  , balanceBalance      :: Float
  , balanceName         :: Maybe String
  , balanceReserved     :: Float
  , balanceUnconfirmed  :: Float
  } deriving (Eq, Show)

instance FromJSON Balance where
  parseJSON (Object o) =
    Balance <$> o .: "account_id"
            <*> o .: "asset"
            <*> (read <$> o .: "balance")
            <*> o .:? "name"
            <*> (read <$> o .: "reserved")
            <*> (read <$> o .: "unconfirmed")

newtype Balances = Balances
  { balances  :: [ Balance ]
  } deriving (Eq, Show)

instance FromJSON Balances where
  parseJSON (Object o) = Balances <$> o .: "balance"

getAccountBalances :: LunoClient -> IO [ Balance ]
getAccountBalances client = do
  result <- lunoAuthenticatedQuery client "1/balance" []
  return $ balances result
